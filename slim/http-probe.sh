#!/bin/sh

# Script will exit in case the curl fails
set -e

#NOTE: with Gitlab dind you need to point at the docker host (not the container ip directly)
#IP_ADDRESS=$(docker inspect -f '{{range.NetworkSettings.Networks}}{{.IPAddress}}{{end}}' $(docker ps | grep $IMAGE_TAG | awk '{print $1}'))
#curl -f http://$IP_ADDRESS:9000/

curl -f http://docker:9000